class SearchController < ApplicationController
  def index
    @q = Article.search(params[:q])
    @articles = @q.result.page(params[:page])
  end
end
