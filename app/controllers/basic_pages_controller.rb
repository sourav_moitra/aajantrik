class BasicPagesController < ApplicationController
  def home
    @articles = Article.order('created_at DESC').limit('30')
    render layout: false
  end

  def about
  end

  def contact
  end
end
