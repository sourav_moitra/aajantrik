class Author < ApplicationRecord
  has_many :articles

  def to_param
    [id, name.parameterize].join("-")
  end
end
