class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :article
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, presence: true, length: { in: 2..20 }, uniqueness: true

  def to_s
    username
  end
end
