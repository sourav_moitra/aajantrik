class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  acts_as_taggable

  belongs_to :author
  mount_uploader :cover_picture, CoverPicUploader
  validates :body, presence: true
  validates :title, presence: true, length: { maximum: 100 }
end
