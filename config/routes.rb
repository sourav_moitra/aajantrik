Rails.application.routes.draw do
  resources :authors
  root 'basic_pages#home'

  get '/search', to: 'search#index'
  get '/about', to: 'basic_pages#about'
  get '/contact', to: 'basic_pages#contact'

  resources :articles
  devise_for :users

  get 'tags/:tag', to: 'articles#index', as: :tag

  mount Ckeditor::Engine => '/ckeditor'
end
