class AddCoverPicToArticles < ActiveRecord::Migration[5.0]
  def change
    add_column :articles, :cover_picture, :string
  end
end
